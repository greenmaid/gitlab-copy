#!/bin/sh

docker run --rm -it \
  -w /code \
  -e CARGO_HOME=/tmp/.cargo \
  -v cargo_index:/tmp/.cargo \
  -v $PWD/src:/code/src \
  -v $PWD/Cargo.toml:/code/Cargo.toml \
  -v $PWD/Cargo.lock:/code/Cargo.lock \
  -v $PWD/docker_target:/code/target \
  rust:buster \
  bash -c "cargo build $@"

  cp -fv docker_target/release/gitlab-copy .
