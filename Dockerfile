# Run https://github.com/clux/muslrust
FROM clux/muslrust as builder
WORKDIR /usr/src

# Download the target for static linking.
# RUN rustup target add x86_64-unknown-linux-musl

# From https://github.com/purpleprotocol/mimalloc_rust/issues/28
# ENV RUSFLAGS='-Clink-arg=-Wl,-Bstatic -Clink-arg=-lc'

# Create a dummy project and build the app's dependencies.
# If the Cargo.toml or Cargo.lock files have not changed,
# we can use the docker build cache and skip these (typically slow) steps.
RUN USER=root cargo new gitlab-copy
WORKDIR /usr/src/gitlab-copy
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release

# Copy the source and build the application.
COPY src ./src
RUN cargo install --path .

# Copy the statically-linked binary into a scratch container.
FROM alpine:3.10

COPY --from=builder /root/.cargo/bin/gitlab-copy /usr/bin/

USER 1001
ENTRYPOINT ["gitlab-copy"]
