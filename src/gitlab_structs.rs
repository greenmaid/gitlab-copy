use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct AccessLevel {
    pub access_level: u32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ProtectedBranch {
    pub id: u32,
    pub name: String,
    pub push_access_levels: Vec<AccessLevel>,
    pub merge_access_levels: Vec<AccessLevel>,
}
#[derive(Serialize, Deserialize, Debug)]
pub struct ProtectedTag {
    pub name: String,
    pub create_access_levels: Vec<AccessLevel>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Group {
    pub name: String,
    pub id: u64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ProjectVariable {
    pub key: String,
    pub value: String,
    pub variable_type: String,
    pub protected: bool,
    pub masked: bool,
    pub environment_scope: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Project {
    pub id: u64,
    pub name: String,
    pub http_url_to_repo: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ProjectConfig {
    pub description: String,
    pub default_branch: String,
    pub visibility: String,
    pub merge_method: String,
    pub ci_config_path: Option<String>,
    pub build_timeout: u32,
    pub lfs_enabled: bool,
    pub container_registry_enabled: bool,
    pub public_jobs: bool,
    pub auto_devops_enabled: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PipelineSchedule {
    pub id: u64,
    pub description: String,
    pub cron: String,
    pub active: bool,
    #[serde(alias = "ref")]
    pub ref_name: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct MergeRequest {
     #[serde(skip_serializing)]
    pub iid: u64,
    pub source_branch: String,
    pub target_branch: String,
    pub title: String,
    pub description: String,
    pub labels: Vec<String>,
    #[serde(rename(serialize = "remove_source_branch", deserialize = "force_remove_source_branch"))]
    pub remove_source_branch: Option<bool>,
    pub squash: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Release {
    pub name: String,
    pub tag_name: String,
    pub description: String,
    pub released_at: String,
}
