use git2::{Cred, RemoteCallbacks, Repository, Error, BranchType::Local};
use retry::retry;
use std::path::Path;

fn get_credentials_callbacks(token: &str) -> RemoteCallbacks {
    // Prepare callbacks.
    let mut callbacks = RemoteCallbacks::new();
    callbacks.credentials(move |_url, _username_from_url, _allowed_types| {
    Cred::userpass_plaintext("oauth2", &token[..])
    });
    callbacks
}

pub fn clone_repo(url: &str, token: &str, directory: &Path) -> Repository {

    let callbacks = get_credentials_callbacks(token);
    // Prepare fetch options.
    let mut fo = git2::FetchOptions::new();
    fo.remote_callbacks(callbacks);

    let repo = git2::build::RepoBuilder::new()
        .bare(true)
        .remote_create(|repo,name,url| repo.remote_with_fetch(name, url, "+refs/*:refs/*"))
        .fetch_options(fo)
        .clone(url, directory).unwrap();

    // // fix to handle bug where buggy ref is created
    // file_ops::rm_directory_if_exists(&format!("{}/refs/remotes", directory));

    repo
}

pub fn push_repo_to_new_url(repo: Repository, remote_url: &str, token: &str) -> Result<(),Error> {
    let mut remote = repo.remote("target", remote_url).unwrap();
    let callbacks = get_credentials_callbacks(token);
    let mut push_options = git2::PushOptions::new();
    push_options
        .remote_callbacks(callbacks)
        .packbuilder_parallelism(0);

    let branches: Vec<String> = repo.branches(Some(Local)).unwrap()
        .filter_map(|result| result.ok())
        .map(|(branch, _)| branch.name().unwrap().unwrap().to_string())
        .map(|branch| format!("+refs/heads/{}", branch))
        .collect();
    for branch in branches {
            println!("  -> pushing branch {}", branch);
            retry(retry::delay::Fixed::from_millis(100).take(3), || {
                remote.push(&[&branch], Some(&mut push_options))
            })
            .unwrap();
    }

    for name in repo.tag_names(None)?.iter() {
        if let Some(tag_name) = name {
            let tag_ref = format!("+refs/tags/{}", tag_name);
            println!("  -> pushing tag {}", tag_name);
            retry(retry::delay::Fixed::from_millis(100).take(3), || {
                remote.push(&[&tag_ref], Some(&mut push_options))
            })
            .unwrap();
        }
    }

    Ok(())

}
