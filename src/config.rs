

pub struct Config {
  pub working_dir: String,
  pub gitlab_source_instance: String,
  pub source_repo_name: String,
  pub source_token: String,

  pub gitlab_target_instance: String,
  pub target_repo_name: String,
  pub target_token: String,

  pub skip_clone_main: bool,
  pub skip_clone_wiki: bool,
}

pub fn get_config_from_app_args(args: clap::ArgMatches) -> Config {

  let (skip_main, skip_wiki) = match args.value_of("skip_clone").unwrap() {
    "all" => (true, true),
    "main" => (true, false),
    "wiki" => (false, true),
    _ => (false, false),
  };

  Config {
      working_dir: args.value_of("working_dir").unwrap().to_string(),
      gitlab_source_instance: args.value_of("gitlab_source_instance").unwrap().to_string(),
      source_repo_name: args.value_of("source_repo_name").unwrap().to_string(),
      source_token: args.value_of("source_token").unwrap().to_string(),
      gitlab_target_instance: args.value_of("gitlab_target_instance").unwrap().to_string(),
      target_repo_name: args.value_of("target_repo_name").unwrap().to_string(),
      target_token: args.value_of("target_token").unwrap().to_string(),
      skip_clone_main: skip_main,
      skip_clone_wiki: skip_wiki,
  }
}
