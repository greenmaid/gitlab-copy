use crate::config::Config;
use crate::gitlab_ops;
use crate::gitlab_structs;
use crate::git_ops;
use git2::Repository;
use gitlab_structs::Project;
use std::fs;
use std::path::Path;


pub fn execute_copy_repo(config: Config) -> Result<(),String> {

    let source_project = match gitlab_ops::get_gitlab_project(&config.gitlab_source_instance, &config.source_repo_name, &config.source_token) {
        Ok(project) => project,
        Err(message) => return Err(format!("Error retrieving source project : {}", message))
    };

    let target_project = match gitlab_ops::get_gitlab_project(&config.gitlab_target_instance, &config.target_repo_name, &config.target_token) {
        Ok(project) => {
            println!("Target project {} already exists", project.http_url_to_repo);
            project
            },
        Err(_) => {
            println!("Create target project {} on https://{}", &config.target_repo_name, &config.gitlab_target_instance);
            gitlab_ops::create_empty_repository(&config.gitlab_target_instance, &config.target_repo_name, &config.target_token).unwrap()
        }
    };
    let target_url = target_project.http_url_to_repo.clone();

    if !(config.skip_clone_main) || !(config.skip_clone_wiki) {
        println!("Change CI file path to avoid unattended pipeline triggering");
        gitlab_ops::apply_fake_ci_config_path(&target_project, &config.target_token).unwrap();
    }

    if !(config.skip_clone_main) {
        println!("Clone source repository locally");
        let main_repo_dir = format!("{}/main", &config.working_dir);
        let main_clone_dir = Path::new(&main_repo_dir);
        let local_repo = match clone_repo_locally(&source_project.http_url_to_repo, &config.source_token, main_clone_dir) {
            Ok(repo) => repo,
            Err(message) => return Err(format!("Error cloning repo : {}", message))
        };

        println!("Pushing main repo to {}", target_url);
        match push_local_repo(local_repo, &target_url, &config.target_token) {
            Ok(_) => (),
            Err(message) => return Err(format!("Error pushin main repo : {}", message))
        };
    };

    if !(config.skip_clone_wiki) {
        println!("Clone wiki repository locally");
        let wiki_repo_dir = format!("{}/wiki", &config.working_dir);
        let wiki_clone_dir = Path::new(&wiki_repo_dir);
        let source_wiki_url = gitlab_ops::get_wiki_url(&source_project.http_url_to_repo);
        let local_wiki_repo = match clone_repo_locally(&source_wiki_url, &config.source_token, wiki_clone_dir) {
            Ok(repo) => repo,
            Err(message) => return Err(format!("Error cloning wiki repo : {}", message))
        };

        let target_wiki_url = gitlab_ops::get_wiki_url(&target_url);
        println!("Pushing wiki repo to {}", target_wiki_url);
        match push_local_repo(local_wiki_repo, &target_wiki_url, &config.target_token) {
            Ok(_) => (),
            Err(message) => return Err(format!("Error pushin wiki repo : {}", message))
        };
    };

    println!("Copying project config");
    match copy_project_config(&source_project, &target_project, &config.source_token, &config.target_token) {
        Ok(_) => (),
        Err(message) => return Err(format!("Error copying project config: {}", message))
    };

    println!("Copying project protected branches");
    match copy_protected_branches(&source_project, &target_project, &config.source_token, &config.target_token) {
        Ok(_) => (),
        Err(message) => return Err(format!("Error copying protected branches: {}", message))
    };

    println!("Copying project protected tags");
    match copy_protected_tags(&source_project, &target_project, &config.source_token, &config.target_token) {
        Ok(_) => (),
        Err(message) => return Err(format!("Error copying protected tags: {}", message))
    };

    println!("Copying project pipeline schedules");
    match copy_pipeline_schedules(&source_project, &target_project, &config.source_token, &config.target_token) {
        Ok(_) => (),
        Err(message) => return Err(format!("Error copying protected tags: {}", message))
    };

    println!("Copying open merge request");
    match copy_open_merge_requests(&source_project, &target_project, &config.source_token, &config.target_token) {
        Ok(_) => (),
        Err(message) => return Err(format!("Error copying merge requests: {}", message))
    };

    println!("Copying release notes");
    match copy_releases(&source_project, &target_project, &config.source_token, &config.target_token) {
        Ok(_) => (),
        Err(message) => return Err(format!("Error copying releases: {}", message))
    };

    println!("Copying project variables");
    match copy_variables(&source_project, &target_project, &config.source_token, &config.target_token) {
        Ok(_) => (),
        Err(message) => return Err(format!("Error copying project variables : {}", message))
    };

    println!("Done !");
    Ok(())
}



///////////////////////////////////////////////////

fn clone_repo_locally(url: &str, token: &str, directory: &Path) -> Result<Repository, String> {
    if directory.exists() {
        fs::remove_dir_all(directory).unwrap();
    }
    let local_repo = git_ops::clone_repo(url, token, directory);
    Ok(local_repo)
}

fn push_local_repo(local_repo: Repository, url: &str, token: &str) -> Result<(), String> {
  match git_ops::push_repo_to_new_url(local_repo, url, token) {
    Ok(_) => Ok(()),
    Err(e) => return Err(format!("Failed to push to target repo : {}", e))
  }
}

fn copy_variables(source_project: &Project, target_project: &Project, source_token: &str, target_token: &str) -> Result<(),String> {
    let variables = match gitlab_ops::get_variable_list_from_api(&source_project, &source_token) {
        Ok(variables) => variables,
        Err(error) => return Err(format!("Error retrieving source project variables: {}", error))
    };
    for var in variables {
        let var_ref = var.key.clone();
        match gitlab_ops::delete_variable_with_api(&target_project, &target_token, &var_ref) {
            Ok(_) => (),
            Err(_) => ()
        }
        match gitlab_ops::create_variable_with_api(&target_project, &target_token, var) {
            Ok(_) => (),
            Err(error) => return Err(format!("Error copying variable {}: {}", var_ref, error))
        }
    }
    Ok(())
}

fn copy_project_config(source_project: &Project, target_project: &Project, source_token: &str, target_token: &str) -> Result<(),String> {
    let source_project_config = match gitlab_ops::get_project_config_from_api(&source_project, source_token) {
        Ok(config) => config,
        Err(e) => panic!(format!("Error retrieving source project configuration: {}", e))
    };

    match gitlab_ops::copy_project_config_with_api(&target_project, target_token, source_project_config) {
        Ok(()) => Ok(()),
        Err(e) => Err(format!("Error copying project config (perhaps check if source project visibility is compatible with target group configuration): {}", e))
    }
}

fn copy_protected_branches(source_project: &Project, target_project: &Project, source_token: &str, target_token: &str) -> Result<(),String> {
    let protected_branches = match gitlab_ops::get_project_protected_branches_from_api(&source_project, source_token) {
        Ok(config) => config,
        Err(e) => return Err(format!("Error retrieving protected branches: {}", e))
    };
    match gitlab_ops::unprotect_branches_with_api(&target_project, target_token) {
        Ok(()) => (),
        Err(e) => return Err(format!("Error unprotecting master branch: {}", e))
    }
    for branch in protected_branches {
        let branch_name = &branch.name.clone();
        match gitlab_ops::delete_all_pipeline_schedules_with_api(&target_project, target_token) {
            Ok(()) => (),
            Err(e) => return Err(format!("Error removing {} from protected branches: {}", branch_name, e))
        }
        match gitlab_ops::protect_branch_with_api(&target_project, target_token, branch) {
            Ok(()) => (),
            Err(e) => return Err(format!("Error defining {} as protected branch: {}", branch_name, e))
        }
    }
    Ok(())
}

fn copy_protected_tags(source_project: &Project, target_project: &Project, source_token: &str, target_token: &str) -> Result<(),String> {
    let protected_tags = match gitlab_ops::get_project_protected_tags_from_api(&source_project, source_token) {
        Ok(config) => config,
        Err(e) => return Err(format!("Error retrieving protected tags: {}", e))
    };
    for tag in protected_tags {
        let tag_name = &tag.name.clone();
        match gitlab_ops::protect_tag_with_api(&target_project, target_token, tag) {
            Ok(()) => (),
            Err(e) => return Err(format!("Error defining {} as protected tag: {}", tag_name, e))
        }
    }
    Ok(())
}

fn copy_pipeline_schedules(source_project: &Project, target_project: &Project, source_token: &str, target_token: &str) -> Result<(),String> {
    let schedules = match gitlab_ops::get_project_pipeline_schedules_from_api(&source_project, source_token) {
        Ok(config) => config,
        Err(e) => return Err(format!("Error retrieving pipeline schedules: {}", e))
    };
    for schedule in schedules {
        let schedule_name = &schedule.description.clone();
        match gitlab_ops::create_pipeline_schedule_with_api(&target_project, target_token, schedule) {
            Ok(()) => (),
            Err(e) => return Err(format!("Error creating schedule {}: {}", schedule_name, e))
        }
    }
    Ok(())
}

fn copy_open_merge_requests(source_project: &Project, target_project: &Project, source_token: &str, target_token: &str) -> Result<(),String> {
    let merge_requests = match gitlab_ops::get_project_opened_merge_request_from_api(&source_project, source_token) {
        Ok(mr) => mr,
        Err(e) => return Err(format!("Error retrieving merge requests: {}", e))
    };

    match gitlab_ops::delete_all_merge_requests_with_api(&target_project, target_token) {
        Ok(_) => (),
        Err(e) => return Err(format!("Error deleting existing merge requests: {}", e))
    };

    for mr in merge_requests {
        let mr_title = &mr.title.clone();
        match gitlab_ops::create_merge_request_with_api(&target_project, target_token, mr) {
            Ok(()) => (),
            Err(e) => return Err(format!("Error creating merge request {}: {}", mr_title, e))
        }
    }
    Ok(())
}

fn copy_releases(source_project: &Project, target_project: &Project, source_token: &str, target_token: &str) -> Result<(),String> {
    let releases = match gitlab_ops::get_project_releases_from_api(&source_project, source_token) {
        Ok(releases) => releases,
        Err(e) => return Err(format!("Error retrieving releases: {}", e))
    };


    match gitlab_ops::delete_all_release_with_api(&target_project, target_token) {
        Ok(_) => (),
        Err(e) => return Err(format!("Error deleting existing releases: {}", e))
    };

    for release in releases {
        let release_name = &release.name.clone();
        match gitlab_ops::create_release_with_api(&target_project, target_token, release) {
            Ok(()) => (),
            Err(e) => return Err(format!("Error creating release {}: {}", release_name, e))
        }
    }
    Ok(())
}
