use crate::gitlab_structs::*;

use serde::de::DeserializeOwned;
use serde::Serialize;
use std::collections::HashMap;
use std::path::Path;
use url::{Url, Position, form_urlencoded::byte_serialize};



// Gitlab URL getters
///////////////////////////////////////////////

pub fn get_wiki_url(repo_url: &str) -> String {
    let base = Path::new(repo_url).ancestors().nth(1).unwrap();
    let repo: &str = Path::new(repo_url).file_stem().unwrap().to_str().unwrap();
    return format!("{}/{}.wiki.git", base.display(), repo)
}

fn get_api_url_from_project(project: &Project) -> String {
    let project_url = Url::parse(&project.http_url_to_repo).unwrap();
    let gitlab_instance = &project_url[Position::BeforeHost..Position::AfterPort];
    get_api_url_from_instance(gitlab_instance)
}

fn get_api_url_from_instance(instance: &str) -> String {
    format!("https://{}/api/v4", instance, )
}

fn get_project_api_url(project: &Project) -> String {
    format!("{}/projects/{}", get_api_url_from_project(project), project.id)
}

fn get_variables_api_url(project: &Project) -> String {
    format!("{}/projects/{}/variables", get_api_url_from_project(project), project.id)
}

fn get_protected_tags_api_url(project: &Project) -> String {
    format!("{}/projects/{}/protected_tags", get_api_url_from_project(project), project.id)
}

fn get_protected_branches_api_url(project: &Project) -> String {
    format!("{}/projects/{}/protected_branches", get_api_url_from_project(project), project.id)
}

fn get_pipeline_schedules_api_url(project: &Project) -> String {
    format!("{}/projects/{}/pipeline_schedules", get_api_url_from_project(project), project.id)
}

fn get_merge_requests_api_url(project: &Project) -> String {
    format!("{}/projects/{}/merge_requests", get_api_url_from_project(project), project.id)
}

fn get_opened_merge_requests_api_url(project: &Project) -> String {
    format!("{}/projects/{}/merge_requests?state=opened", get_api_url_from_project(project), project.id)
}

fn get_releases_api_url(project: &Project) -> String {
    format!("{}/projects/{}/releases", get_api_url_from_project(project), project.id)
}


// Generic requests
//////////////////////////////////////////


pub fn get_object_list_from_api<T>(api_url: &str, token: &str) -> Result<Vec<T>, reqwest::Error>
    where T: DeserializeOwned {
    let client = reqwest::blocking::Client::new();
    let response = client
        .get(api_url)
        .header("PRIVATE-TOKEN", token)
        .query(&[("per_page", "100")])
        .send()?
        .error_for_status()?;
    let objects: Vec<T> = response.json()?;
    Ok(objects)
}

pub fn get_single_object_from_api<T>(api_url: &str, token: &str) -> Result<T, reqwest::Error>
    where T: DeserializeOwned {
    let client = reqwest::blocking::Client::new();
    let response = client
        .get(api_url)
        .header("PRIVATE-TOKEN", token)
        .send()?
        .error_for_status()?;
    let objects: T = response.json()?;
    Ok(objects)
}

pub fn create_new_object_with_params_with_api<T>(api_url: &str, token: &str, params: HashMap<&str,String>) -> Result<T, reqwest::Error>
    where T: DeserializeOwned {
    let client = reqwest::blocking::Client::new();
    let response = client
        .post(api_url)
        .header("PRIVATE-TOKEN", token)
        .form(&params)
        .send()?
        .error_for_status()?;
    let new_object: T = response.json()?;
    Ok(new_object)
}

pub fn create_object_from_json_struct_with_api<T>(api_url: &str, token: &str, object: T) -> Result<(), reqwest::Error>
    where T: Serialize  {
    let client = reqwest::blocking::Client::new();
    let _response = client
        .post(api_url)
        .header("PRIVATE-TOKEN", token)
        .json(&object)
        .send()?
        .error_for_status()?;
    Ok(())
}


pub fn create_object_with_form_struct_with_api(api_url: &str, token: &str, params: HashMap<&str, String>) -> Result<(), reqwest::Error> {
    let client = reqwest::blocking::Client::new();
    // println!("Params : {:#?}", params);
    let _response = client
        .post(api_url)
        .header("PRIVATE-TOKEN", token)
        .form(&params)
        .send()?
        .error_for_status()?;
    Ok(())
}

pub fn delete_objet_with_api(api_url: &str, token: &str) -> Result<(), reqwest::Error> {
    let client = reqwest::blocking::Client::new();
    let _response = client
        .delete(api_url)
        .header("PRIVATE-TOKEN", token)
        .send()?
        .error_for_status()?;
    Ok(())
}



// Gitlab objects getters
//////////////////////////////////////

pub fn get_gitlab_project(instance: &str, repo_name: &str, token: &str) -> Result<Project, reqwest::Error> {
    let repo_urlencoded: String = byte_serialize(repo_name.as_bytes()).collect();
    let instance_api_url = get_api_url_from_instance(instance);
    let api_url = format!("{}/projects/{}", instance_api_url, repo_urlencoded);
    get_single_object_from_api(&api_url, token)
}

pub fn get_project_pipeline_schedules_from_api(project: &Project, token: &str) -> Result<Vec<PipelineSchedule>, reqwest::Error> {
    let api_url = get_pipeline_schedules_api_url(project);
    get_object_list_from_api(&api_url, token)
}

pub fn get_variable_list_from_api(project: &Project, token: &str) -> Result<Vec<ProjectVariable>, reqwest::Error> {
    let api_url = get_variables_api_url(project);
    get_object_list_from_api(&api_url, token)
}

pub fn get_project_config_from_api(project: &Project, token: &str) -> Result<ProjectConfig, reqwest::Error> {
    let api_url = get_project_api_url(project);
    get_single_object_from_api(&api_url, token)
}

pub fn get_project_protected_branches_from_api(project: &Project, token: &str) -> Result<Vec<ProtectedBranch>, reqwest::Error> {
    let api_url = get_protected_branches_api_url(project);
    get_object_list_from_api(&api_url, token)
}

pub fn get_gitlab_namespace(instance: &str, namespace_name: &str, token: &str) -> Result<Group, reqwest::Error> {
    let urlencoded: String = byte_serialize(namespace_name.as_bytes()).collect();
    let instance_api_url = get_api_url_from_instance(instance);
    let api_url = format!("{}/groups/{}", instance_api_url, urlencoded);
    get_single_object_from_api(&api_url, token)
}

pub fn get_project_protected_tags_from_api(project: &Project, token: &str) -> Result<Vec<ProtectedTag>, reqwest::Error> {
    let api_url = get_protected_tags_api_url(project);
    get_object_list_from_api(&api_url, token)
}

pub fn get_project_opened_merge_request_from_api(project: &Project, token: &str) -> Result<Vec<MergeRequest>, reqwest::Error> {
    let api_url = get_opened_merge_requests_api_url(project);
    get_object_list_from_api(&api_url, token)
}

pub fn get_project_releases_from_api(project: &Project, token: &str) -> Result<Vec<Release>, reqwest::Error> {
    let api_url = get_releases_api_url(project);
    get_object_list_from_api(&api_url, token)
}




// Gitlab object creators
/////////////////////////////////

pub fn create_release_with_api(project: &Project, token: &str, release: Release) -> Result<(), reqwest::Error> {
    let api_url = get_releases_api_url(project);
    let tag_name = release.tag_name.clone();
    match create_object_from_json_struct_with_api(&api_url, token, release) {
        Ok(_) => Ok(()),
        Err(e) => match e.status() {
            Some(status) => match status.as_u16() {
                422 => {println!("  > Release for {} is unprocessable (tag not valid). Skipping", tag_name); Ok(())},
                _ => Err(e)
            }
            _ => Err(e),
        }
    }
}

pub fn create_merge_request_with_api(project: &Project, token: &str, mr: MergeRequest) -> Result<(), reqwest::Error> {
    let api_url = get_merge_requests_api_url(project);
    create_object_from_json_struct_with_api(&api_url, token, mr)
}

pub fn create_variable_with_api(project: &Project, token: &str, variable: ProjectVariable) -> Result<(), reqwest::Error> {
    let api_url = get_variables_api_url(project);
    create_object_from_json_struct_with_api(&api_url, token, variable)
}

pub fn create_pipeline_schedule_with_api(project: &Project, token: &str, schedule: PipelineSchedule) -> Result<(), reqwest::Error> {
    let api_url = get_pipeline_schedules_api_url(project);
    let mut params = HashMap::new();
        params.insert("description", schedule.description);
        params.insert("cron", schedule.cron);
        params.insert("active", schedule.active.to_string());
        params.insert("ref", schedule.ref_name);
    create_object_from_json_struct_with_api(&api_url, token, params)
}

pub fn create_empty_repository(instance: &str, project_name: &str, token: &str) -> Result<Project,  reqwest::Error> {
    let (opts_namespace, project)= split_repo_name_with_namespace(project_name);
    let mut params = HashMap::new();
        params.insert("path", project.to_string());
    if let Some(namespace_name) = opts_namespace {
        let namespace_id = get_gitlab_namespace(instance, &namespace_name, token)
            .unwrap().id.to_string();
        params.insert("namespace_id", namespace_id);
    }
    let instance_api_url = get_api_url_from_instance(instance);
    let api_url = format!("{}/projects", instance_api_url);
    create_new_object_with_params_with_api(&api_url, token, params)
}


pub fn protect_branch_with_api(project: &Project, token: &str, protected_branch: ProtectedBranch) ->  Result<(), reqwest::Error> {
    let api_url = get_protected_branches_api_url(project);
    let mut params = HashMap::new();
        params.insert("name", protected_branch.name);
        params.insert("push_access_level", protected_branch.push_access_levels[0].access_level.to_string());
        params.insert("merge_access_level", protected_branch.merge_access_levels[0].access_level.to_string());
    create_object_from_json_struct_with_api(&api_url, token, params)?;
    Ok(())
}

pub fn protect_tag_with_api(project: &Project, token: &str, protected_tag: ProtectedTag) ->  Result<(), reqwest::Error> {
    let api_url = get_protected_tags_api_url(project);
    let mut params = HashMap::new();
        params.insert("name", protected_tag.name);
        params.insert("create_access_level", protected_tag.create_access_levels[0].access_level.to_string());
    create_object_from_json_struct_with_api(&api_url, token, params)?;
    Ok(())
}


// Gitlab object updates
/////////////////////////////

pub fn apply_fake_ci_config_path(project: &Project, token: &str) ->  Result<(), reqwest::Error> {
    let mut params = HashMap::new();
            params.insert("ci_config_path", "fake_file_preventing_pipeline_triggering_when_pushing.yml");
    change_project_parameters(&project, token, params)
}

pub fn change_project_parameters(project: &Project, token: &str, parameters: HashMap<&str, &str>) ->  Result<(), reqwest::Error> {
    let client = reqwest::blocking::Client::new();
    let api_url = get_project_api_url(project);
    let _response = client
        .put(&api_url)
        .header("PRIVATE-TOKEN", token)
        .form(&parameters)
        .send()?
        .error_for_status()?;
  Ok(())
}

pub fn copy_project_config_with_api(project: &Project, token: &str, project_config: ProjectConfig) ->  Result<(), reqwest::Error> {
    let client = reqwest::blocking::Client::new();
    let api_url = get_project_api_url(project);
    let _response = client
        .put(&api_url)
        .header("PRIVATE-TOKEN", token)
        .json(&project_config)
        .send()?
        .error_for_status()?;
  Ok(())
}


// Gitlab object deleters
/////////////////////////////////////

pub fn unprotect_branches_with_api(project: &Project, token: &str) ->  Result<(), reqwest::Error> {
    let protected_branches = get_project_protected_branches_from_api(project, token).unwrap();
    for branch in protected_branches {
        let api_url = format!("{}/{}", get_protected_branches_api_url(project), branch.name);
        delete_objet_with_api(&api_url, token)?;
    }
    Ok(())
}

pub fn delete_all_pipeline_schedules_with_api(project: &Project, token: &str) ->  Result<(), reqwest::Error> {
    let pipeline_schedules = get_project_pipeline_schedules_from_api(project, token).unwrap();
    for schedule in pipeline_schedules {
        let api_url = format!("{}/{}", get_pipeline_schedules_api_url(project), schedule.id);
        delete_objet_with_api(&api_url, token)?;
    }
    Ok(())
}

pub fn delete_all_merge_requests_with_api(project: &Project, token: &str) ->  Result<(), reqwest::Error> {
    let merge_requests = get_project_opened_merge_request_from_api(project, token).unwrap();
    for mr in merge_requests {
        let api_url = format!("{}/{}", get_merge_requests_api_url(project), mr.iid);
        delete_objet_with_api(&api_url, token)?;
    }
    Ok(())
}

pub fn delete_all_release_with_api(project: &Project, token: &str) ->  Result<(), reqwest::Error> {
    let releases = get_project_releases_from_api(project, token).unwrap();
    for release in releases {
        let api_url = format!("{}/{}", get_releases_api_url(project), release.tag_name);
        delete_objet_with_api(&api_url, token)?;
    }
    Ok(())
}

pub fn delete_variable_with_api(project: &Project, token: &str, variable_key: &str) -> Result<(), reqwest::Error> {
    let api_url = format!("{}/{}", get_variables_api_url(project), variable_key);
    delete_objet_with_api(&api_url, token)
}

// Misc tools
//////////////////////////

fn split_repo_name_with_namespace(repo_name: &str) -> (Option<String>, &str) {
    let splitted_repo_name = repo_name.split('/');
    let counted = splitted_repo_name.clone().count();
    let name = splitted_repo_name.clone().last().unwrap();
    if counted > 1 {
        let mut repo_vec: Vec<&str> = splitted_repo_name.collect();
            repo_vec.remove(counted - 1);
        return (Some(repo_vec.join("/")), name)
    }
    return (None, name)
}


///////////////////
// tests
////////////////////////

#[cfg(test)]
mod tests {
  use super::*;

    #[test]
    fn test_get_wiki() {
        let repo = "https://gitlab.com/me/test.git";
        let wiki_repo = "https://gitlab.com/me/test.wiki.git";
        assert_eq!(get_wiki_url(repo),wiki_repo);
    }

    #[test]
    fn test_get_api() {
      let project = Project {
        id: 20,
        name: "mygroup/myrepo".to_string(),
        http_url_to_repo: "https://gitlab.com/mygroup/myrepo.git".to_string(),
      };
      assert_eq!(get_variables_api_url(&project), "https://gitlab.com/api/v4/projects/20/variables".to_string());

      let project2 = Project {
        id: 21,
        name: "mygroup/myrepo".to_string(),
        http_url_to_repo: "https://gitlab.com:8443/mygroup/myrepo.git".to_string(),
      };
      assert_eq!(get_variables_api_url(&project2), "https://gitlab.com:8443/api/v4/projects/21/variables".to_string());

    }

}
