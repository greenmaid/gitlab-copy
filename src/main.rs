
mod copy_logic;
mod git_ops;
mod gitlab_ops;
mod gitlab_structs;
mod config;
use config::Config;
use clap::{App, Arg, crate_version};


fn main() {

    let application = App::new("Gitlab migrator")
        .about("Copy a gitlab project to another gitlab instance")
        .author("Mederic de Verdilhac <med2ver@gmail.com>")
        .version(crate_version!());

    let arg_working_dir = Arg::with_name("working_dir")
        .short("w")
        .long("workdir")
        .env("GITLAB_COPY_WORKDIR")
        .value_name("DIRECTORY")
        .help("directory where source repository will be clone (deleted after usage)")
        .takes_value(true)
        .default_value("/tmp");

    let arg_gitlab_source_instance = Arg::with_name("gitlab_source_instance")
        .long("source-instance")
        .env("GITLAB_COPY_SOURCE_INSTANCE")
        .value_name("GITLAB_INSTANCE")
        .help("Gitlab instance url (without 'https') hosting the source repo")
        .takes_value(true)
        .default_value("gitlab.com");

    let arg_gitlab_target_instance = Arg::with_name("gitlab_target_instance")
        .long("target-instance")
        .env("GITLAB_COPY_TARGET_INSTANCE")
        .value_name("GITLAB_INSTANCE")
        .help("Gitlab instance url (without 'https') for hosting the target repo")
        .takes_value(true)
        .default_value("gitlab.com");

    let arg_source_repo_name = Arg::with_name("source_repo_name")
        .long("source-name")
        .env("GITLAB_COPY_SOURCE_REPO")
        .value_name("PROJECT")
        .help("Path/name of the source project")
        .takes_value(true)
        .required(true);

    let arg_target_repo_name = Arg::with_name("target_repo_name")
        .long("target-name")
        .env("GITLAB_COPY_TARGET_REPO")
        .value_name("PROJECT")
        .help("Path/name of the target project")
        .takes_value(true)
        .required(true);

    let arg_source_token = Arg::with_name("source_token")
        .long("source-token")
        .env("GITLAB_COPY_SOURCE_TOKEN")
        .value_name("TOKEN")
        .help("Gitlab token for cloning the source project")
        .takes_value(true)
        .required(true);

    let arg_target_token = Arg::with_name("target_token")
        .long("target-token")
        .env("GITLAB_COPY_TARGET_TOKEN")
        .value_name("TOKEN")
        .help("Gitlab token for creating the target project")
        .takes_value(true)
        .required(true);

    let skip_clone = Arg::with_name("skip_clone")
        .long("skip-clone")
        .value_name("VALUE")
        .help("Bypass git clone/push steps (clone, wiki or all)")
        .takes_value(true)
        .default_value("none")
        .validator(is_valid_skip_value)
        .required(false);

    let app_arguments = application
        .arg(arg_working_dir)
        .arg(arg_gitlab_source_instance)
        .arg(arg_source_repo_name)
        .arg(arg_source_token)
        .arg(arg_gitlab_target_instance)
        .arg(arg_target_repo_name)
        .arg(arg_target_token)
        .arg(skip_clone)
        .get_matches();

    let config: Config = config::get_config_from_app_args(app_arguments);

    copy_logic::execute_copy_repo(config).expect("failed");

}

fn is_valid_skip_value(value: String) -> Result<(), String> {
    let valid_values = [
        String::from("none"),
        String::from("main"),
        String::from("wiki"),
        String::from("all"),
        ];
    match valid_values.contains(&value) {
        true => Ok(()),
        false => Err(format!("value {} is not valid", value))
    }
}
